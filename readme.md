
Dotfiles Installer
======================

Largelyy a ripoff of https://github.com/michaeljsmalley/dotfiles.  Thanks Michael!

See Michael's readme at https://github.com/michaeljsmalley/dotfiles/blob/master/README.markdown

Installation
=======================
git clone https://bitbucket.org/0xBDB/dotfiles.git ~/dotfiles
cd ~/dotfiles
./install.sh

Things I've added
-----------------------

Additional aliases, change to my preferred zsh theme, etc.  For some of the alaises to work you will need to install fzf, grc, bat.

Things to add
-----------------------

Ansible integration to install a much larger set of tools, particularly for pentesters.


